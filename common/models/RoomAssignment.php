<?php

namespace common\models;

use \common\models\base\RoomAssignment as BaseRoomAssignment;

/**
 * This is the model class for table "room_assignment".
 */
class RoomAssignment extends BaseRoomAssignment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['year', 'semester', 'student_id', 'room_id'], 'required'],
            [['year', 'semester', 'student_id', 'room_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'], 'safe']
        ]);
    }
	
}
