<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "students".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $ic_no
 * @property string $email
 * @property string $address
 * @property string $phone_no
 * @property string $spm
 * @property integer $status
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Parents[] $parents
 * @property \common\models\RoomAssignment[] $roomAssignments
 */
class Students extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'ic_no', 'email'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'ic_no', 'email', 'address', 'phone_no', 'spm'], 'string', 'max' => 255],
            [['ic_no'], 'unique'],
            [['email'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'ic_no' => 'Ic No',
            'email' => 'Email',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'spm' => 'Spm',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(\common\models\Parents::className(), ['student_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomAssignments()
    {
        return $this->hasMany(\common\models\RoomAssignment::className(), ['student_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\StudentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\StudentsQuery(get_called_class());
    }
}
