<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "room".
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property string $no
 * @property integer $status
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Hostel $hostel
 * @property \common\models\RoomAssignment[] $roomAssignments
 */
class Room extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostel_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['no'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['no'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hostel_id' => 'Hostel ID',
            'no' => 'No',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostel()
    {
        return $this->hasOne(\common\models\Hostel::className(), ['id' => 'hostel_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomAssignments()
    {
        return $this->hasMany(\common\models\RoomAssignment::className(), ['room_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\RoomQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RoomQuery(get_called_class());
    }
}
