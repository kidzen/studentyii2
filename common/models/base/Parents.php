<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "parents".
 *
 * @property integer $id
 * @property string $relationship
 * @property integer $student_id
 * @property string $name
 * @property string $ic_no
 * @property string $email
 * @property string $address
 * @property string $phone_no
 * @property integer $status
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Students $student
 */
class Parents extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'name', 'ic_no', 'email'], 'required'],
            [['student_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['relationship', 'name', 'ic_no', 'email', 'address', 'phone_no'], 'string', 'max' => 255],
            [['ic_no'], 'unique'],
            [['email'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parents';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'relationship' => 'Relationship',
            'student_id' => 'Student ID',
            'name' => 'Name',
            'ic_no' => 'Ic No',
            'email' => 'Email',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(\common\models\Students::className(), ['id' => 'student_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ParentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ParentsQuery(get_called_class());
    }
}
