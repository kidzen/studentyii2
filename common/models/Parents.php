<?php

namespace common\models;

use \common\models\base\Parents as BaseParents;

/**
 * This is the model class for table "parents".
 */
class Parents extends BaseParents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['student_id', 'name', 'ic_no', 'email'], 'required'],
            [['student_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['relationship', 'name', 'ic_no', 'email', 'address', 'phone_no'], 'string', 'max' => 255],
            [['ic_no'], 'unique'],
            [['email'], 'unique']
        ]);
    }
	
}
