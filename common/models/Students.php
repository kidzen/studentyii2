<?php

namespace common\models;

use \common\models\base\Students as BaseStudents;

/**
 * This is the model class for table "students".
 */
class Students extends BaseStudents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'ic_no', 'email'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'ic_no', 'email', 'address', 'phone_no', 'spm'], 'string', 'max' => 255],
            [['ic_no'], 'unique'],
            [['email'], 'unique']
        ]);
    }
	
}
