<?php

namespace common\models;

use \common\models\base\Room as BaseRoom;

/**
 * This is the model class for table "room".
 */
class Room extends BaseRoom
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['hostel_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['no'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['no'], 'string', 'max' => 255]
        ]);
    }
	
}
