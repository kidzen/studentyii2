<?php

namespace common\models;

use \common\models\base\Role as BaseRole;

/**
 * This is the model class for table "role".
 */
class Role extends BaseRole
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
}
