<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role_id' => $this->integer()->defaultValue(1),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(0),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->insertData();
        $this->createTable('{{%students}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'name' => $this->string()->notNull(),
            'ic_no' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'address' => $this->string(),
            'phone_no' => $this->string(),
            'spm' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%parents}}', [
            'id' => $this->primaryKey(),
            'relationship' => $this->string(),
            'student_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'ic_no' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'address' => $this->string(),
            'phone_no' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%hostel}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%room}}', [
            'id' => $this->primaryKey(),
            'hostel_id' => $this->integer(),
            'no' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%room_assignment}}', [
            'id' => $this->primaryKey(),
            'year' => $this->integer()->notNull(),
            'semester' => $this->integer()->notNull(),
            'student_id' => $this->integer()->notNull(),
            'room_id' => $this->integer()->notNull(),
            'start_date' => $this->dateTime(),
            'end_date' => $this->dateTime(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('fk_user_role_id', '{{%user}}', 'role_id', '{{%role}}', 'id');
        $this->addForeignKey('fk_parents_student_id', '{{%parents}}', 'student_id', '{{%students}}', 'id');
        $this->addForeignKey('fk_room_hostel_id', '{{%room}}', 'hostel_id', '{{%hostel}}', 'id');
        $this->addForeignKey('fk_room_assignment_student_id', '{{%room_assignment}}', 'student_id', '{{%students}}', 'id');
        $this->addForeignKey('fk_room_assignment_room_id', '{{%room_assignment}}', 'room_id', '{{%room}}', 'id');
    }

    public function insertData() {
        $this->insert('{{%role}}', [
            'name' => 'Administrator',
            'description' => 'This user can control all.',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%user}}', [
            'username' => 'superadmin',
            'auth_key' => \Yii::$app->security->generateRandomString(),
            'password_hash' => \Yii::$app->security->generatePasswordHash('superadmin'),
            'email' => 'admin1',
            'role_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    public function down() {
        $this->dropForeignKey('fk_parents_student_id', '{{%parents}}');
        $this->dropForeignKey('fk_room_hostel_id', '{{%room}}');
        $this->dropForeignKey('fk_room_assignment_room_id', '{{%room_assignment}}');
        $this->dropForeignKey('fk_room_assignment_student_id', '{{%room_assignment}}');
        $this->dropForeignKey('fk_user_role_id', '{{%user}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%role}}');
        $this->dropTable('{{%students}}');
        $this->dropTable('{{%parents}}');
        $this->dropTable('{{%hostel}}');
        $this->dropTable('{{%room}}');
        $this->dropTable('{{%room_assignment}}');
    }

}
