<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hostel Management System</h1>

        <p class="lead">System application for students registration and hostility management.</p>

        <p><a class="btn btn-lg btn-success" href="<?php yii\helpers\Url::to('/site/index') ?>">Get started</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Student Management</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?php echo \yii\helpers\Html::a('Student List &raquo;',[yii\helpers\Url::to('/students/index')],['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Hostel Management</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?php echo \yii\helpers\Html::a('Hostel List &raquo;',[yii\helpers\Url::to('/hostel/index')],['class'=>'btn btn-default']) ?></p>
                <!--<p><a class="btn btn-default" href="<?php yii\helpers\Url::to('/hostel/index') ?>">Yii Forum &raquo;</a></p>-->
            </div>
            <div class="col-lg-4">
                <h2>Room Assignment</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?php echo \yii\helpers\Html::a('Room Registration &raquo;',[yii\helpers\Url::to('/room-assignment/create')],['class'=>'btn btn-default']) ?></p>
                <!--<p><a class="btn btn-default" href="<?php yii\helpers\Url::to('/room-assignment/create') ?>">Yii Extensions &raquo;</a></p>-->
            </div>
        </div>

    </div>
</div>
