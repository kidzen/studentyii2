<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = 'Save As New Hostel: '. ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Hostel', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="hostel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
