<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = 'Create Hostel';
$this->params['breadcrumbs'][] = ['label' => 'Hostel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hostel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
