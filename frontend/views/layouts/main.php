<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => '<strong style="color:white;">Hostel Management System</strong>',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems1 = [
//                ['label' => 'Parent', 'url' => ['/parents/index']],
                [
                    'label' => 'Student',
                    'items' => [
                        ['label' => 'Register', 'url' => ['/students/create'],],
                        ['label' => 'Student list', 'url' => ['/students/index'],],
                        ['label' => 'Parent', 'url' => ['/parents/create'],],
                    ],
                ],
                [
                    'label' => 'Hostel',
                    'items' => [
                        ['label' => 'Registration', 'url' => ['/room-assignment/create'],],
                        ['label' => 'Room List', 'url' => ['/room/index'],],
                        ['label' => 'Hostel List', 'url' => ['/hostel/index'],],
                        ['label' => 'Room Assignment List', 'url' => ['/room-assignment/index'],],
                    ],
                ],
                [
                    'label' => 'Administration',
                    'items' => [
                        ['label' => 'User List', 'url' => ['/user-list/index'],],
                        ['label' => 'Role Management', 'url' => ['/role/index'],],
                    ],
                ],
//                ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav'],
                    'items' => $menuItems1,
                ]);
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>


            <div class="container">
                <div class="row">
                    <!--<div class="col-md-3">-->
                    <?php
//                echo $this->render('sidenav');
                    ?>
                    <!--</div>-->
                    <div class="col-md-12">
                        <!--<div class="col-md-9">-->
                        <?=
                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
