<?php

use kartik\widgets\SideNav;
// OR if this package is installed separately, you can use
// use kartik\sidenav\SideNav;
use yii\helpers\Url;

$item = Yii::$app->controller->getRoute();
// var_dump($item);die();
echo SideNav::widget([
    'type' => 'primary',
    'encodeLabels' => false,
    // 'heading' => true,
    'items' => [
        // Important: you need to specify url as 'controller/action',
        // not just as 'controller' even if default action is used.
        //
        // NOTE: The variable `$item` is specific to this demo page that determines
        // which menu item will be activated. You need to accordingly define and pass
        // such variables to your view object to handle such logic in your application
        // (to determine the active status).
        //

    ['label' => 'Home', 'icon' => 'home', 'url' => Url::to(['/site/index', 'type' => 'primary']), 'active' => ($item == 'site/index')],
        ['label' => 'Students', 'icon' => 'user', 'items' => [
                ['label' => '<span class="pull-right badge">10</span> Register', 'url' => Url::to(['/students/create', 'type' => 'primary']), 'active' => ($item == 'students/create')],
                ['label' => '<span class="pull-right badge">5</span> Most Popular', 'url' => Url::to(['/site/most-popular', 'type' => 'primary']), 'active' => ($item == 'most-popular')],
                ['label' => 'Read Online', 'icon' => 'cloud', 'items' => [
                        ['label' => 'Online 1', 'url' => Url::to(['/site/online-1', 'type' => 'primary']), 'active' => ($item == 'online-1')],
                        ['label' => 'Online 2', 'url' => Url::to(['/site/online-2', 'type' => 'primary']), 'active' => ($item == 'online-2')]
                    ]],
            ]],
        ['label' => '<span class="pull-right badge">3</span> Categories', 'icon' => 'tags', 'items' => [
                ['label' => 'Fiction', 'url' => Url::to(['/site/fiction', 'type' => 'primary']), 'active' => ($item == 'fiction')],
                ['label' => 'Historical', 'url' => Url::to(['/site/historical', 'type' => 'primary']), 'active' => ($item == 'historical')],
                ['label' => '<span class="pull-right badge">2</span> Announcements', 'icon' => 'bullhorn', 'items' => [
                        ['label' => 'Event 1', 'url' => Url::to(['/site/event-1', 'type' => 'primary']), 'active' => ($item == 'event-1')],
                        ['label' => 'Event 2', 'url' => Url::to(['/site/event-2', 'type' => 'primary']), 'active' => ($item == 'event-2')]
                    ]],
            ]],
        ['label' => 'Profile', 'icon' => 'user', 'url' => Url::to(['/site/profile', 'type' => 'primary']), 'active' => ($item == 'profile')],
    ],
]);
?>