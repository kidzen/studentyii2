<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\RoomAssignment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Room Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-assignment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Room Assignment'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'year',
        'semester',
        [
                'attribute' => 'student.name',
                'label' => 'Student'
            ],
        [
                'attribute' => 'room.no',
                'label' => 'Room'
            ],
        'start_date',
        'end_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
