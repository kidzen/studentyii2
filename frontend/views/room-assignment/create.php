<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RoomAssignment */

$this->title = 'Create Room Assignment';
$this->params['breadcrumbs'][] = ['label' => 'Room Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-assignment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
